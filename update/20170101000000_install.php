<?
class wp_user__20170101000000_install
{
	function __construct(){ global $C, $D; $this->C = &$C; $this->D = &$D; }
	function __call($m, $a){ return $a[0]; } 
	
	function up()
	{
		$this->C->db()->query("CREATE TABLE `user_right` (
		  `user_id` varchar(32) NOT NULL,
		  `right_id` varchar(32) NOT NULL,
		  `active` tinyint(1) DEFAULT NULL,
		  `user_right` int(11) DEFAULT NULL,
		  `utimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  `itimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");

		$this->C->db()->query("CREATE TABLE `user_user` (
		  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
		  `active` tinyint(1) DEFAULT NULL,
		  `nickname` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
		  `email` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
		  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
		  `utimestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  `itimestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");

		$this->C->db()->query("ALTER TABLE `user_right` ADD PRIMARY KEY (`user_id`,`right_id`);");
		$this->C->db()->query("ALTER TABLE `user_user` ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `email` (`email`);");
		return 1;
	}
	
	function down()
	{
		return 1;
	}
}