<?
class wp_user__admin__userlist extends wp_user__admin__userlist__parent
{
	function load($d = null)
	{
		parent::{__function__}();
		
		$this->C->user()->check_right(['RIGHT'=>'ADMIN']);
		switch($this->D['ACTION'])
		{
			default:
				$this->C->user()->get_user();
		}
	}
	
	function show($d=null)
	{
		$this->C->library()->smarty()->assign('D', $this->D);
		$this->C->library()->smarty()->display(__dir__.'/tpl/admin__userlist.tpl');
	}
}