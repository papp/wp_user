<?
class wp_user__admin__admin extends wp_user__admin__admin__parent
{
	function load($d = null)
	{
		parent::{__function__}();
		$this->D['NAVIGATION']['D']['LEFT']['D']['USER']['LANGUAGE']['D']['DE'] = [ 'AWESOME_ICON'	=> 'fa-user', 'TITLE' => 'Benutzer'];
		$this->D['NAVIGATION']['D']['LEFT']['D']['USER']['D']['USER']['LANGUAGE']['D']['DE'] = array(
			'AWESOME_ICON'	=> 'fa-user',
			'TITLE' => 'Benutzer',
			'LINK'	=> 'D[PAGE]=admin__userlist',
		);
		$this->D['NAVIGATION']['D']['LEFT']['D']['USER']['D']['NEWUSER']['LANGUAGE']['D']['DE'] = array(
			'TITLE'			=> 'Neuer Benutzer',
			'LINK'			=> 'D[PAGE]=admin__user&D[ACTION]=new',
			'AWESOME_ICON'	=> 'fa-user-plus',
		);
	}
}