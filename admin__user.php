<?
class wp_user__admin__user extends wp_user__admin__user__parent
{
	function load($d = null)
	{
		parent::{__function__}();
		
		$this->C->user()->check_right(['RIGHT'=>'ADMIN']);
		switch($this->D['ACTION'])
		{
			case 'new':
				$ID = md5(microtime());
				#$this->D['MODUL']['D']['wp_user']['USER']['D'][$ID]['ACTIVE'] = 1;
				$this->D['USER']['D'][$ID]['ACTIVE'] = 1;
				break;
			case 'set_user':
				$this->C->user()->set_user();
				break;
			default:
				$this->C->user()->get_user();
		}
	}
	
	function show($d=null)
	{
		$this->C->library()->smarty()->assign('D', $this->D);
		$this->C->library()->smarty()->display(__dir__.'/tpl/admin__user.tpl');
	}
}