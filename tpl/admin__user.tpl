
	<form id="form">
		<div class="tab-content">
			
			<ul class="nav nav-tabs" role="tablist">
				<li class="active"><a href="#0" data-toggle="tab">User</a></li>
				<!--<li><a href="#0" data-toggle="tab">Rechte</a></li>-->
			</ul>
			
			<div class="panel-body tab-pane active" id="0">
				{foreach from=$D.USER.D name=USE item=USE key=kUSE}
				<table class="table">
					<thead>
						<tr>
							<td>Title</td>
							<td>Wert</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>id</td>
							<td>{$kUSE}</td>
						</tr>
						<tr>
							<td>Active</td>
							<td><input class="form-control" name="D[USER][D][{$kUSE}][ACTIVE]" value="{$USE.ACTIVE}"></td>
						</tr>
						<tr>
							<td>Nickname</td>
							<td><input class="form-control" name="D[USER][D][{$kUSE}][NICK]" value="{$USE.NICK}"></td>
						</tr>
						<tr>
							<td>E-Mail</td>
							<td><input class="form-control" name="D[USER][D][{$kUSE}][EMAIL]" value="{$USE.EMAIL}"></td>
						</tr>
						<tr>
							<td>Passwort</td>
							<td><input class="form-control" name="D[USER][D][{$kUSE}][PASSWORD]" value=""></td>
						</tr>
					</tbody>
				</table>
				{/foreach}
			</div>

			<div class="panel-footer text-right">
				<div class="btn-group">
					<button type="button" onclick="$.ajax({ type: 'POST', dataType : 'json', url : '?D[PAGE]=admin__user&D[ACTION]=set_user', data : $('#form').serialize() });" class="btn btn-default">Speichern</button>
					<button type="button" onclick="$('#page').load('?D[PAGE]=admin__userlist'); return false;" class="btn btn-default">Abbrechen</button>
				</div>
			</div>
		</div>
	</form>


