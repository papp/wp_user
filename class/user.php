<?
class wp_user__class__user extends wp_user__class__user__parent
{	
	function get_user($d=null)
	{
		$W = $this->C->db()->where_interpreter(array(
			'ID'			=> "id IN ('ID')",
			'NICK'			=> "nickname LIKE 'NICK'",
			'EMAIL'			=> "email LIKE 'EMAIL'",
			'ACTIVE'		=> "active = 'ACTIVE'",
		),$this->D['USER']['W']);
		$ary = $this->C->db()->query("SELECT id, active, nickname,email, password, utimestamp, itimestamp FROM user_user WHERE 1 {$W} {$ORDER} {$LIMIT}");
	
		foreach($ary as $k => $v)
		{
			$this->D['USER']['D'][ $v['id'] ] = array(
				'ACTIVE'	=> $v['active'],
				'NICK'		=> $v['nickname'],
				'EMAIL'		=> $v['email'],
				'PASSWORD'	=> $v['password'],#password_hash('', PASSWORD_DEFAULT),
				'UTIMESTAMP'=> $v['utimestamp'],
				'ITIMESTAMP'=> $v['itimestamp'],
			);
		}

		$ID = implode("','",(array)array_keys((array)$this->D['USER']['D']));
		$ary = $this->C->db()->query("SELECT user_id, right_id, active, user_right, utimestamp, itimestamp 
									FROM user_right WHERE user_id IN ('{$ID}')");
		foreach($ary as $k => $v)
		{
			$this->D['USER']['D'][ $v['user_id'] ]['RIGHT']['D'][ $v['right_id'] ] = array(
				'ACTIVE'	=> $v['active'],
				'RIGHT'		=> $v['user_right'],
				'UTIMESTAMP'=> $v['utimestamp'],
				'ITIMESTAMP'=> $v['itimestamp'],
				#'ROOT'	=> null,
				#'ADMIN'	=> null,
				#'USER'	=> 1,
				#'GAST'	=> 1,
			);
		}
		#ROOT	ADMIN	USER	GAST
		#
		#4 Lesen
		#2 Schreiben
		#1 Ausführen
	}
	
	function set_user($d=null)
	{
		foreach((array)$this->D['USER']['D'] as $k => $v )
		{
			if($v['ACTIVE'] != -1)
			{
				$IU .= (($IU)?',':'')."('{$k}'";
				$IU .= (isset($v['ACTIVE']))?", '{$v['ACTIVE']}'":", NULL";
				$IU .= (isset($v['NICK']))?", '{$v['NICK']}'":", NULL";
				$IU .= (isset($v['EMAIL']))?", '{$v['EMAIL']}'":", NULL";
				$IU .= (isset($v['PASSWORD']) && $v['PASSWORD'] != '')?", '".password_hash($v['PASSWORD'], PASSWORD_DEFAULT)."'":", NULL";#password_hash($D['LOGIN']['PASSWORD'], PASSWORD_DEFAULT)
				$IU .= ")";
			}
			else
				$DEL .= ($DEL?',':'')."'{$k}'";
		}
		
		if($IU)
			$this->C->db()->query("INSERT INTO user_user (id, active, nickname, email, password) VALUES {$IU}
									ON DUPLICATE KEY UPDATE
									active = CASE WHEN VALUES(active) IS NOT NULL THEN VALUES(active) ELSE user_user.active END,
									nickname = CASE WHEN VALUES(nickname) IS NOT NULL THEN VALUES(nickname) ELSE user_user.nickname END,
									email = CASE WHEN VALUES(email) IS NOT NULL THEN VALUES(email) ELSE user_user.email END,
									password = CASE WHEN VALUES(password) IS NOT NULL THEN VALUES(password) ELSE user_user.password END
									");
									
		if($DEL)
			$this->C->db()->query("DELETE FROM user_user WHERE id IN ({$DEL})");
	}
	
	function check_right($d=null)
	{
		$this->D['SESSION']['LOGIN']['BACK_REDIRECT'] = '?D[PAGE]='.$this->D['PAGE'];#'?D[PAGE]=admin';#ToDo: param automatisch auslesen
		if(!$this->D['SESSION']['USER'])
		{
			header("Location: ?D[PAGE]=indexlogin");
		}#ToDo: Rechte
		/*
		elseif($this->D['SESSION']['USER']['RIGHT'] !== $d['RIGHT'])
		{
			#ToDo: RIGHT insert ignore in die Datenbank hinzufügen
			header("Location: ?D[PAGE]=indexlogin");
		}*/
	}
} 